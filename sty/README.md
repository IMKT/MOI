# The LaTeX package `moi`.

**Math Object Identifiers** (MOIs: digital object identifiers for mathematical concepts, objects, and models; see the [MOI paper](../doc/lwda17/paper.pdf)) constitute a very lightweight form of semantic annotation that can support many knowledge-based workflows in mathematics, e.g. classification of articles via the objects mentioned or object-based search. In essence MOIs are an enabling technology for Linked Open Data for mathematics and thus makes (parts of) the mathematical literature into mathematical research data.

To realistically obtain enough MOI registrations, we need to integrate MOI registration into the scientific publication and review processes. The `moi` package provides a LaTeX-based infrastructure for that.

## Package User Interface

In a nutshell (see [the documentation](moi.pdf) for details), use
* `\usepackage{moi}` to enable MOI functionality; the `show` option shows MOI metadata in
  the margin, the `publish` option removes all functionalities, and makes all MOI markup invisible. 
* `\rmoi[<opts>]{<frag>}` proposes a text fragment `<frag>` for registrastion via your publisher. This is invisible in the PDF, but the publisher can extract the data. `\begin`/`\end{rmoienv}` is the environment version (for larger fragments).
* `\moiref[⟨moi⟩]{⟨frag⟩}` annotates a text fragment ⟨frag⟩ that references a mathematical object by an existing ⟨moi⟩. `\begin`/`\end{moirefenv}` is the environment version (for larger fragments).

## Practical Usage

If your paper introduces a new mathematical object e.g.

```LaTeX
The \textbf{functional object ontology} (also called a \textbf{FOO} is ...
```
then you annotate it in the LaTeX sources as
```LaTeX
\begin{rmoienv}[id=foo]
The \textbf{functional object ontology} (also called a \textbf{FOO} is ...
\end{rmoienv}
```
which will give the reviewers and editors the information that you propose this as a MOI registration. If the paper and the MOI registration is accepted, then the editors can generate additions to the MOI registry (semi)-automatically and - with lots of MOIRegistration-annotated publications - arrive at a meaning full MOI-based ontology.

See the [LaTeX source](test.tex), the [generated PDF](test.pdf), and the [generated XML](test.xml) of the system test example from the distribution.

## Building the Package
`moi.sty` is provided as a self-documenting LaTeX package [`moi.dtx`];
 run `make` to generate the documentation [`moi.pdf`](moi.pdf)  and [`moi.sty`](moi.sty).


