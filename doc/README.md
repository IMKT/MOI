This directory contains some documents explaining the MOI ideas.

* `proposal`: a living document for the MOI proposal (updated regularly to keep it up-to-date with current thinking)
* `lwda17`: a published paper (having a deadline helps clarify thoughts)
