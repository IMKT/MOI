\documentclass{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[show]{ed}
%\usepackage{calbf}
\usepackage{a4wide}
\usepackage{amstext,amsmath,amssymb}
\usepackage{array,arydshln}
\usepackage{mathtools}
\usepackage{xspace}
\usepackage{paralist}
\usepackage{listings}\lstset{basicstyle=\sf\footnotesize,columns=fullflexible}
\setcounter{tocdepth}{3}
\usepackage[bookmarks,linkcolor=red,citecolor=blue,urlcolor=gray,colorlinks,breaklinks,bookmarksopen,bookmarksnumbered]{hyperref}
\setlength{\hfuzz}{3pt}
\hbadness=10001 %make box warning less stric

\usepackage[hyperref,backend=bibtex,style=alphabetic]{biblatex}
\addbibresource{kwarcpubs.bib}
\addbibresource{extpubs.bib}
\addbibresource{kwarccrossrefs.bib}
\addbibresource{extcrossrefs.bib}

\DeclarePairedDelimiter\floor{\lfloor}{\rfloor}
\DeclarePairedDelimiter\ceil{\lceil}{\rceil}
\def\meta#1{\ensuremath{\langle\kern-.2em\langle}#1\ensuremath{\rangle\kern-.2em\rangle}}

\title{Math Object Identifiers -- Towards Research Data in Mathematics}
\author{Michael Kohlhase\\
Computer Science, FAU Erlangen-N\"urnberg}
\begin{document}
\maketitle
\begin{abstract}
  We propose to develop a system of ``Math Object Identifiers'' (MOIs: digital object
  identifiers for mathematical concepts, objects, and models) and a process of registering
  them. These envisioned MOIs constitute a very lightweight form of semantic annotation
  that can support many knowledge-based workflows in mathematics, e.g. classification of
  articles via the objects mentioned or object-based search. In essence MOIs are an
  enabling technology for Linked Open Data for mathematics and thus makes (parts of) the
  mathematical literature into mathematical research data.

  This document represent the current thinking of the author(s) on the topic and
  realization of MOIs. It is updated regularly to synchronize; the respective current
  version can be found at
  \url{https://gitlab.com/IMKT/MOI/blob/master/doc/proposal/proposal.pdf}.
\end{abstract}

\section{Introduction}

The last years have seen a surge in interest in scaling computer support in scientific
research by preserving, making accessible, and managing research data. For most subjects,
research data consist in measurement or simulation data about the objects of study,
ranging from subatomic particles via weather systems to galaxy clusters.

Mathematics has largely been left untouched by this trend, since the objects of study --
mathematical concepts, objects, and models -- are by and large abstract and their
properties and relations apply whole classes of objects. There are some exceptions to
this, concrete integer sequences, finite groups, or $\ell$-functions and modular form are
collected and catalogued in mathematical data bases like the OEIS (Online Encyclopedia of
Integer Sequences)~\cite{oeis, Sloane:oeis12}, the GAP Group
libraries~\cite[Chap. 50]{Gap-RefMan:on}, or the LMFDB ($\ell$-Functions and Modular Forms
Data Base)~\cite{lmfdb:on,Cremona:LMFDB16}.

Abstract mathematical structures like groups, manifolds, or probability distributions can
formalized -- usually by definitions -- in logical systems, and their relations expressed
in form of theorems which can be proved in the logical systems as well. Today there are
about half-a-dozen libraries with $\sim10^5$ formalized definitions, theorems, and proofs;
e.g.  the libraries of Mizar \cite{mizar}, Coq \cite{CoqManual}, and the HOL systems
\cite{Harrison:hlti96}.  These include deep theorems such as the Odd-Order
Theorem~\cite{GonAspAiv:mcpoot13} or the Kepler Conjecture~\cite{HalAdaBau:afpkc15}.

Finally, the mathematical literature is systematically collected in the two mathematical
abstracting services Zentralblatt Math~\cite{zbMATH:on} and Math Reviews
\cite{MathSciNet:on}, which also (jointly) classify more than 120,000 articles in the
Mathematics Subject Classification (MSC)~\cite{MSC2010} annually.

Unfortunately, while all of these individually constitute steps into the direction of
research data, they attack the problem at different levels (object, vs. document level) and
direction (description- vs. classification-based) and are mutually incompatible and
not-interlinked/aligned systematically. 

Finally, only the abstracting systems manage to keep up with the extent (3.5 M articles
since 1860) and growth (120,000 articles annually) of the mathematical literature.  As a
consequence they constitute the only full-coverage information systems for mathematical
knowledge. Unfortunately, the MSC classification alone does not give sufficiently focused
access to the literature -- the time where a working mathematician could ``subscribe'' to
a dozen MSC classes and stay up to date by scanning/reading all articles in them are over:
many of the almost 8000 MSCs have more than a hundred articles appearing
annually. Incidentally, searches in the zbMATH and MathSciNet databases are mostly by
bibliographic metadata, such as authors, years, and keywords -- not by MSC classes or the
mathematical concepts, objects, or models the user is interested in. Even the new formula
search tab in zbMATH~\cite{KohMihSperTes:mfs13}, which does give access to formulae does
not help much in this situation, since the concept of formula search and the query
interface is unfamiliar to most users.

Experience from other scientific fields show us that this intuition that research data and
information systems should be about the objects of science is adequate, and indeed why a
large infrastructure around these has sprung up. Unfortunately, full formalization in
logics and even partial/flexible formalization~\cite{Kohlhase:tffm13} as developed by the
author's research group that would enable that is currently intractable in practice. In
this situation, we propose to develop and deploy an open, and community-based system
\emph{mathematical object identifiers} (MOIs), i.e. handles on mathematical objects that
allow to uniquely reference mathematical concepts, objects, and models. Such a system of
MOIs (and a central information system that exposes them to the user) would simplify many
mathmeatical information gathering and knowledge management tasks.

This document represent the current thinking of the author(s) on the topic and realization
of MOIs. It is updated regularly; an earlier version was published
as~\cite{Kohlhase:moitrdm17}.

In the next section we discuss scientific referencing systems that use object or document
handles. In Section~\ref{sec:moi-proposal} we develop a concrete proposal for registering
math object identifiers. In Section~\ref{sec:appl} we sketch some applications of MOIs
that justify our claim of information simplification. Section~\ref{sec:concl} concludes
the paper.

\section{Handle Systems for Scientific Documents and Objects}\label{sec:handles}

Document Object Identifiers~\cite{DOI:on} are persistent digital identifiers for objects
(any object really), but are predominantly used for media (normally documents of some
kind). DOIs are registered for virtually all scientific publications nowadays and allow
location-independent and persistent referencing of the scientific literature and a host of
knowledge management applications on top of this.  Services like the DOI catalog at
\url{https://doi.org} allow to look publications by their DOI, and the bibliometry and
citation analysis profit from the unique handle system.

But (scientific) publications are only a means to the end of conveying information and
knowledge about the objects of science. Thus a finer-grained handle system for the objects
of science would benefit scientific knowledge management and information retrieval:
Publications could be annotated by the objects they talk about and could be indexed by the
handles. Indeed, some sciences have developed such handle systems: The most prominent are
probably the InChI (IUPAC International Chemical Identifier)~\cite{InChI:on} system in
chemistry, which allows to reference any substance by a 27-character string and the CAS
registry numbers (CASRN)~\cite{CASregistry:on} in chemistry. Both systems are widely
deployed and are used for referencing and knowledge management (both commercial and
public).

These systems profit from the fact that chemistry has a fixed domain -- all substances can
be built compositionally from a finite set of elements -- and the community has developed
a standardized way to describe substances by their chemical formulae (the IUPAC
nomenclature) that has been essentially fixed for a century now. The InChIs are directly
computed from the formula and associated information.

Mathematics works differently, instead of an external domain fixed by nature, mathematics
studies the domain of all objects that can be rigorously described by axiomatizations,
definitions, and theorems. The method of scientific investigation is that of proof rather
than experimentation, modeling, and simulation. Crucially, there is no standardized
nomenclature for mathematical objects, only a weakly standardized language of making
descriptions. In particular, different descriptions may refer to the same mathematical
object -- we call them \textbf{equivalent} -- and there is no intrinsic way of preferring
any of them, since all of them add different insights to understanding this object.

At times, equivalence of descriptions is immediate, in other cases can require deep
insights and decades of work to establish. Analogously it may be very hard to tell
mathematical objects apart, an extreme example is the two integer sequences
$\floor*{\frac{2n}{log(2)}}$ and $\ceil*{\frac{2}{2^{1/n}-1}}$ that match for
777451915729367 terms but are not equal~\cite{Sloane:oeis12}. Another one is the
$\mathbf{P}$ versus $\mathbf{NP}$ problem, where currently do not know whether the classes
of problems that can be answered ($\mathbf{P}$) or checked ($\mathbf{NP}$) in polynomial
time are equal or not. Indeed, this question is one of the most prominent unsolved
problems in theoretical Computer Science.

Therefore it seems intractable to give handles to the ``platonic mathematical objects'' --
even though they have subjective reality to mathematicians -- and develop a system of
handles for published descriptions instead and treat mathematical equivalence -- i.e. if
two descriptions refer to the same platonic mathematical object -- as a metadata relation
that can be added over time.

As there cannot be a normative nomenclature for mathematical objects, we propose to model
the math object identifiers after the CAS registry numbers rather than the InChIs from
chemistry.

\section{The MOI Proposal}\label{sec:moi-proposal}

I propose that the mathematical community -- possibly together with a consortium of
scientific publishers -- establishes a \textbf{MOI Registry Organization} and process that
registers MOIs and provides an open information system about them. This organization and
service could be hosted by the newly founded International Mathematical Knowledge Trust
(IMKT)~\cite{IMKT:on}, the mathematical abstracting services, the IMU~\cite{IMU:on}, or
even the OpenMath Society~\cite{OMSoc:on}.

Like the CAS Registry, the MOI registry should assign numbers called \textbf{math object
  numbers} (MONs) in sequential order\footnote{Crucially, MONs should not be construed to
  carry mathematical information. In particular the MONs should be independent of any
  classification system of mathematical objects such as the MSC or other systematic
  schemes.} to (sufficiently unique descriptions of) mathematical objects identified by
the mathematical community for inclusion in the database. From these numbers the MOI
registry derives \textbf{Math Object Identifiers} (MOIs): unique string identifiers
(hashes) that may contain check digits or related safety measures.

In principle any \emph{persistent, peer-reviewd description} of a mathematical concept,
object, or model is eligible for registration as long as it is sufficiently different from
already registered ones. We restrict ourselves to mathematical objects whose descriptions
have been published in some kind of persistent, peer-reviewed medium for three reasons:
\begin{compactenum}[\em i\rm)]
\item the set of mathematical objects, even the named ones is infinite (it includes the
  natural numbers),
\item we want to select out the set of objects that are of interest to the mathematical
  community -- interesting enough to have passed peer review, and
\item unless they have been published in a persistent -- i.e. immutable -- medium, we
  cannot guarantee that MOIs form persistent references
\end{compactenum}

Upon registration of a MOI, the Registry establishes a \textbf{Math Object Record} (MOR),
which contains contains the mandatory fields
\begin{compactenum}
\item the math object number (\texttt{MON}), 
\item the registration date (\texttt{MORDate})
\item a normative reference to the media fragment that constitutes the MO description --
  we call this a \textbf{math object description reference} (\texttt{MODRef}).
\end{compactenum}
Other metadata can be added directly in the MOR or by linked-open-data methods, but is not
considered normative. We will give examples in the next section.

The exact criteria for ensuring peer-review and persistence -- the latter may well include
versioned \texttt{MODRef}s into versioned media -- should be determined by the MOI
registry organization.

\section{MOI Examples}\label{sec:ex}

Examples of eligible descriptions include theorems, definitions, proofs, and examples,
published in mathematical journals (see Figure~\ref{fig:doimoi}), objects in (informal)
mathematical databases like the OEIS or the LMFDB (see Figure~\ref{fig:archmoi}),
formalizations in a theorem prover library (Figure~\ref{fig:tplmoi}) or locally described
MOs (Figure~\ref{fig:localmoi}). We will now go through these in more detail to build up
our intuition about the issues involved.

\subsection{DOI-based Math Object Records}\label{sec:doimoi}

The most important class of mathematical concepts, objects, and models is established by
publishing (about) them in mathematical articles. Figure~\ref{sec:doimoi} shows three math
object records from an article published by Gregor Fels and Wilhelm Kaup in Acta Math in
2008.

\begin{figure}[h!]\centering\footnotesize
  \begin{tabular}{|>{\tt}l|l|}\hline
    MON & 4711\\
    MOIDate & 2017-11-1 \\
    MODRef &  DOI: 10.1007/s11511-008-0029-0 \# Definition 3.4\\\hdashline
    Type &  Definition\\
    BibRef & Acta Math., 201 (2008), 1–82 p. 12\\
    See &  \url{http://projecteuclid.org/download/pdf_1/euclid.acta/1485891992}\\\hdashline
    Snippet & \includegraphics[width=10.5cm]{MOI34}\\\hline\hline
    MON & 4712\\
    MOIDate & 2017-11-1 \\
    MODRef &  DOI: 10.1007/s11511-008-0029-0 \# Corollary 3.6\\\hdashline
    Type &  Theorem\\
    BibRef & Acta Math., 201 (2008), 1–82 p. 13\\
    See &  \url{http://projecteuclid.org/download/pdf_1/euclid.acta/1485891992}\\
    Note & A condition that a tube manifold is non-degenerate.\\\hdashline
    Snippet & \includegraphics[width=10.5cm]{MOI36}\\\hline\hline
    MON & 4713\\
    MOIDate & 2017-11-2 \\
    MODRef &  DOI: 10.1007/s11511-008-0029-0 \# proof of Corollary 3.6\\\hdashline
    Type &  Proof\\
    BibRef & Acta Math., 201 (2008), 1–82 p. 13\\
    See &  \url{http://projecteuclid.org/download/pdf_1/euclid.acta/1485891992}\\
    Note & a simple three-line proof.\\\hdashline
    Snippet & \includegraphics[width=10.5cm]{MOI36p}\\\hline
  \end{tabular}
  \caption{DOI-based Math Object Records}\label{fig:doimoi}
\end{figure} 

As specified above, the MORs contain the math object number (MON), the MOI registration
date, and a math object description reference (MODRef) -- the mandatory data above the
first dashed line -- as well as additional optional information, such as the object type,
a link to an online version of the article, and an image of the referenced description.

Like Uniform Resource Locator-based references, the MODRefs consist of a \textbf{document
  reference} (here a DOI) and a \textbf{fragment identifier} separated by the \#
character. Only that due to the nature of the target document -- at worst a printed
document, on average a PDF, and at best a structured XML document -- we have to allow for
rigorous references that are not directly machine-actionable. Here we have a PDF and use
human-readable reference to a numbered statement, alternatives would be to use references
based on page/line/column numbers. Note that the first MOR contains a definition of an
object ($K_a^rF$), which is not given name in the article. Nonetheless, we can give it a
MON, and thus make if uniquely referencible. The other two examples are similar in
structure, but concern a theorem and a proof.

The ``additional information'' in the MORecords in Figure~\ref{fig:doimoi} is mostly such
that would make a MOR information system more useful. Such a system would allow to search
the MOI database, there the ``snippet'' would be useful, since it allow the user to
determine the nature of the object identified by the MOI. Other information would be to
record where the MOI is ``used'' and a list of MOIs used in the fragment. E.g. having the
MOIs in the definiendum, would directly give us a (conceptual) dependency relation on
MOIs.

Note that MO descriptions need not be fully formal, but they should be sufficiently
rigorous to be useful (for human readers). For instance the ones in
Figure~\ref{fig:doimoi} are clearly informal, but rigorous. 

\subsection{Archive-Based MOIs}\label{sec:moi-arch}

The next large group of mathematical objects are ones collected and published in
mathematical archives and libraries. Figure~\ref{fig:archmoi} shows two examples, one from
the Online Encyclopedia of Integer Sequences (OEIS) and the second from the LMFDB, the
database of L-functions, modular forms, and related objects. Here, the MO References
consist of the library name and the internal identifier there. Given that most
mathematical libraries are web-accessible nowadays, these directly correspond to a URL or
URL reference (given via the \texttt{MOURL} here). 

Note that the MO descriptions in these mathematical data bases are informal, e.g. all the
integer sequences in the OEIS (see the first example in Figure~\ref{fig:archmoi}):
sequences are given by their initial segment (usually around 50 elements if available) and
further described by metadata about publications, names, implementations, and even
generating functions. The latter have the potential for formality, but are given in an
informal and ambiguous ASCII representation -- see~\cite{LuzKoh:fsarfo16} for details.

\begin{figure}[ht]\centering
  \begin{tabular}{|l|l|}\hline
    MON & 0815 \\
    MODRef &  \url{OEIS.org}  \# A000045 \\
    MOIDate & 2017-11-1 \\\hdashline
    Type &  Integer Sequence\\
    Keywords & Fibonacci Sequence\\
    MOURL &  \url{https://oeis.org/A000045}\\\hline\hline
    MON & 0816\\
    MOIDate & 2017-11-1 \\
    MODRef &  \url{lmfdb.org} \# Elliptic Curve over Q 100a2 (lmfdb label)\\\hdashline
    Type &  Object\\
    MOURL &  \url{http://www.lmfdb.org/EllipticCurve/Q/102/a/2}\\\hline
  \end{tabular}
  \caption{MOR based on informal Archives}\label{fig:archmoi} 
\end{figure}

In the informally based MOIs (from the literature or math data bases in
Figures~\ref{fig:doimoi} and \ref{fig:archmoi}), we have only glossed the MODRefs, so that
they can be understood by humans. The MO Registry Organization will need to standardize
machine-actionable fragment references for MORefs that target digital objects (in PDFs,
scans, or retro-digitized documents). We envision that this will be an open ended list of
MORef types specified in separate standards documents developed when the need arises.

In libraries of formal objects, e.g. in theorem prover libraries we can make use of the
uniform referencing systems by MMT URIs developed by the author's research
group. Figure~\ref{fig:tplmoi} shows an example: MMT URIs are triples consisting of a URI
(the namespace of the library), the theory specifier, and a symbol path -- all separated
by the \lstinline|?| operator. We have transformed the libraries of more than a dozen
theorem provers into the OMDoc/MMT format and made them accessible in the MathHub
system~\cite{MathHub:on,IanJucKoh:sdm14} via their MMT URI; see~\cite{MueGauKal:cacfms17}
for details. These MMT URIs could become a basis for a MORef standard to be developed by
the MOI registry organization.

\begin{figure}[ht]\centering
  \begin{tabular}{|l|l|}\hline
    MON & 31415\\
    MOIDate & 2017-11-1 \\
    MODRef &  \url{http://pvs.csl.sri.com/Prelude?list_props?append}\\\hdashline
    Type &  Object\\
    See &  \url{http://mathhub.info/PVS/Prelude/list_props.omdoc}\\\hline
  \end{tabular}
  \caption{MOR from Theorem Prover Libraries}\label{fig:tplmoi} 
\end{figure}

\subsection{Locally Defined Math Objects}\label{sec:localmoi}

Finally, we could even think of allowing ``locally defined'' MOIs, where the MODRef is a
full description of the MOI in question (see Figure~\ref{fig:localmoi}); but this would
necessitate the standardization of a suitable representation language. The OpenMath
Standard~\cite{BusCapCar:2oms04} would be a candidate, but we would need to restrict
ourselves to the openmath dialect with persistent content dictionaries, e.g. the core
OpenMath CDs~\cite{omcd-core:on}.

\begin{figure}[ht]\centering
  \begin{tabular}{|l|l|}\hline
    MON & 27182\\
    MOIDate & 2017-11-1 \\
    MODRef &  The \meta{MO1} of \meta{MO2} and \meta{MO3}\\\hdashline 
    Type &  Object\\
    Note &  The direct sum (\meta{MO2}) of two specific math objects given by MOs.\\\hline\hline
    MON & 27183\\
    MOIDate & 2017-11-1 \\
    MODRef &  \lstinline|thm = |- !x y s. x IN y INSERT s <=> x = y \/ x IN s|\\
    Type &  theorem\\
    Note &  a fully formal description (HOL Light)\\\hline
  \end{tabular}
  \caption{MORs with locally described MOs}\label{fig:localmoi}
\end{figure}

\subsection{MOI Non-Examples and Corner Cases}

In contrast to all the cases above, mathematical objects in software systems, like
e.g. \lstinline|numpy:mandelbrot| should not be registered as MOIs themselves, since they
are not persistent (e.g between software versions). Instead they can be linked to MOIs,
for instance the numpy manual could use the MOI of the Mandelbrot set as part of the
description of the respective function. A sufficiently exact published description --
e.g. in~\cite[edition 1]{Bressert:snod12} which could give rise to a MOI.

Another example of a legitimate mathematical object that should not (systematically) be
registered is ``the 4711th zero of the $\zeta$ function'' -- there are just too many
zeros.  Unless of course the 4711th one is of particular mathematical interest, but then
we would expect it to be discussed in an article, which would warrant a DOI-based MOI as
shown above.

An interesting corner case is constituted by objects which do not exist (platonically),
such as ``the greatest prime''. Such ``objects'' routinely occur in proofs by
contradiction, and sometimes interesting mathematical concepts are discussed, conjectured
about, and even subject to theorems and proofs long before they are sufficiently
well-defined. The ``Euler characteristic'' of a polyhedron is a point in case, where the
story is a long and protracted one, mostly about ``objects that do not exist (as it turns
out)''~\cite{Lakatos76}.

\section{MOI Applications and Management}\label{sec:appl}

Given a sufficiently large set of registered MOIs and MOI-based information systems that
allow to mathematical practitioners to identify and access MOIs many knowledge management
services become possible 

\begin{enumerate}
\item \emph{Document disambiguation and wikification}: If text fragments are annotated with their
  MOI, then we can add links to their MO record or to their MORef target. This allows the
  reader to disambiguate otherwise context-dependent usages.
\item \emph{MOI-based search engines}: find documents by the objects they talk about
\item \emph{Linked Open Data for Maths}: if the objects are referenced using MOIs in
  papers, then this data can be spidered and papers can be cross-referenced by objects.
\item \emph{mathematical concordances}: we can register MOIs for all the non-equivalent
  variants of a mathematical object -- e.g. elementary functions with different branch
  cuts~\cite{EngBraDav:ubce13} -- and link other objects (e.g. implementations) to the
  respective MOI.
\end{enumerate}
The last application is somewhat characteristic of the change MOIs bring to knowledge
management in mathematics: many workflows become star-shaped. Whereas a concordance
between $n$ systems, needs contributors to be specialists in at least two idiosyncratic
systems -- better in all $n$, in a MOI-based world, contributors only need to know their
own system and be able to find/identify the MOIs; a much more likely skillset.

We see that \emph{MOIs are an enabling technology/resource}! But we have to acknowledge
that there is a chicken-and-egg problem with MOI registration, just as with all other
semantic technologies: even though the joining costs are smaller than for other semantic
technologies, there is no incentive to do it until there is already a large network of
MOIs to connect to.  An additional problem of the proposal above is that the basic MOI
registration process relies on human mathematicians, which creates a bottleneck on the
curation side as well.

Both of these can possibly be alleviated by integrating MOI registration with the
scientific publication and review process: When an author submits a paper for publication,
she also submits a list of MOI registration candidates which can then be reviewed (and
iterated) and approved by the reviewers and submitted by the publishers. This process
could be supported by suitably extended {\LaTeX} macros and environments. For instance the
theorem environments from the \lstinline|amsthm| package could be extended, so that the
author can add a macro like \lstinline[language=TeX]|\moi{foo}| to the environment, from
which the publisher can first generate a MOI registry application and later semantic
annotations in electronic versions of the eventual publication (once the MOI is reviewed
and registered).

This ``organizational solution'' which minimizes problems by putting them into a context
where the information is ``at our fingertips'' can be replaced or supported by methods
from linked-open-data, computational linguistics, or AI/machine learning.

\section{Ongoing Development}\label{sec:development}

We are currently working on the \emph{central MOI information system} hinted at above. We
will seed this with the examples from Section~\ref{sec:ex} (these can be found at
\url{https://gitlab.com/IMKT/MOI/tree/master/registration_data}) and deploy it publically
at \url{http://moi.mathweb.org}, so that we can experiment with registration workflows,
data models, and information visualization. The data collected there may or may not be
used as a seed for an organizational deployment.

Another current work item is the review and registration support via a {\LaTeX}
package. That can be found at \url{https://gitlab.com/IMKT/MOI/tree/master/sty}.


\section{Conclusion \& Future Work}\label{sec:concl}

We have proposed a handle-based system of ``math object identifiers'' to simplify and
scale the management of mathematical knowledge and enhance systems that treat mathematical
knowledge as research data. We have sketched a few applications that show the usefulness
of such a resource. The next step will be to find a host organization for a MOI registry,
to implement a prototype MOI information system, and build some of the applications in
Section~\ref{sec:appl} to show the benefits of the proposal. The latter is feasible, since
we can bootstrap the MOI system with archive-based MOIs from the OEIS and LMFDB, as well
as formal MOIs from the theorem prover libraries in the MathHub system. Furthermore, we
can experiment with informal MORefs from the NNEXUS system~\cite{GinCor:nnexus:14}.


\paragraph{Acknowledgements}
This work has been partially funded by DFG under Grant KO 2428/13-1.  The author
gratefully acknowledges discussions with the KWARC group and the IMU GDML working group.

\printbibliography
\end{document}
%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:

%  LocalWords:  maketitle oeis Sloane:oeis12 Gap-RefMan:on lmfdb:on,Cremona:LMFDB16 mizar
%  LocalWords:  formalized Harrison:hlti96 GonAspAiv:mcpoot13 HalAdaBau:afpkc15 zbMATH:on
%  LocalWords:  MathSciNet:on zbMATH MathSciNet KohMihSperTes:mfs13 sec:handles DOI:on
%  LocalWords:  bibliometry InChI:on CASregistry:on standardized axiomatizations textbf
%  LocalWords:  Sloane:oeis12,oeis Cremona:LMFDB16,lmfdb:on centering hline fig:doimoi
%  LocalWords:  lmfdb fig:archmoi lstinline thm fig:localmoi numpy:mandelbrot numpy emph
%  LocalWords:  formalizations printbibliography Kohlhase:tffm13 sec:moi-proposal IMKT:on
%  LocalWords:  sec:concl organization IMU:on OMSoc:on peer-reviewd fig:tplmoi llncs
%  LocalWords:  standardize retro-digitized MathHub:on,IanJucKoh:sdm14 MueGauKal:cacfms17
%  LocalWords:  LuzKoh:fsarfo16 standardization BusCapCar:2oms04 EngBraDav:ubce13 amsthm
%  LocalWords:  sec:appl mathbf mathbf sec:ex sec:doimoi hdashline includegraphics Leyer
%  LocalWords:  referencible sec:moi-arch omcd-core:on Bressert:snod12 thefootnote
%  LocalWords:  organizational realization synchronize Kohlhase:moitrdm17 publically
%  LocalWords:  visualization
