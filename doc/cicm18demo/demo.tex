\documentclass{llncs}
\usepackage[show]{ed}
%\usepackage{calbf}
\usepackage{amstext,amsmath,amssymb}
\usepackage{array,arydshln}
\usepackage{mathtools}
\usepackage{xspace}
\usepackage{paralist}
\usepackage{listings}\lstset{basicstyle=\sf\footnotesize,columns=fullflexible}
\setcounter{tocdepth}{3}
\usepackage[bookmarks,linkcolor=red,citecolor=blue,urlcolor=gray,colorlinks,breaklinks,bookmarksopen,bookmarksnumbered]{hyperref}
\setlength{\hfuzz}{3pt}
\hbadness=10001 %make box warning less stric

\usepackage[hyperref,backend=bibtex,style=alphabetic]{biblatex}
\addbibresource{kwarcpubs.bib}
\addbibresource{extpubs.bib}
\addbibresource{kwarccrossrefs.bib}
\addbibresource{extcrossrefs.bib}

\DeclarePairedDelimiter\floor{\lfloor}{\rfloor}
\DeclarePairedDelimiter\ceil{\lceil}{\rceil}
\def\meta#1{\ensuremath{\langle\kern-.2em\langle}#1\ensuremath{\rangle\kern-.2em\rangle}}

\title{Demo: Math Object Identifiers -- Towards Research Data in Mathematics}
\author{Michael Kohlhase}
\institute{Computer Science, FAU Erlangen-N\"urnberg}
\begin{document}
\maketitle

\section{Introduction}
The last years have seen a surge in interest in scaling computer support in scientific
research by preserving, making accessible, and managing research data. For most subjects,
research data consist in measurement or simulation data about the objects of study,
ranging from subatomic particles via weather systems to galaxy clusters.

Mathematics has largely been left untouched by this trend, since the objects of study --
mathematical concepts, objects, and models -- are by and large abstract and their
properties and relations apply whole classes of objects. There are some exceptions to
this, concrete integer sequences, finite groups, or $\ell$-functions and modular form are
collected and catalogued in mathematical data bases.  Abstract mathematical structures
like groups, manifolds, or probability distributions can formalized -- usually by
definitions -- in logical systems, and their relations expressed in form of theorems which
can be proved in the logical systems as well. Today there are about half-a-dozen libraries
with $\sim10^5$ formalized definitions, theorems, and proofs; Finally, the mathematical
literature is systematically collected in the two mathematical abstracting services
Zentralblatt Math and Math Reviews, which also (jointly) classify more than 120,000
articles in the Mathematics Subject Classification (MSC) annually.

Unfortunately, while all of these individually constitute steps into the direction of
research data, they attack the problem at different levels (object, vs. document level) and
direction (description- vs. classification-based) and are mutually incompatible and
not-interlinked/aligned systematically. 


Experience from other scientific fields show us that research data and information systems
should be about the objects of science, and indeed why a large infrastructure around these
has sprung up. In this situation, we propose to develop and deploy an open, and
community-based system \emph{mathematical object identifiers} (MOIs), i.e. handles on
mathematical objects that allow to uniquely reference mathematical concepts, objects, and
models. Such a system of MOIs (and a central information system that exposes them to the
user) would simplify many mathmeatical information gathering and knowledge management
tasks.

\section{The MOI Proposal}\label{sec:moi-proposal}

I propose that the mathematical community -- possibly together with a consortium of
scientific publishers -- establishes a \textbf{MOI Registry Organization} and process that
registers MOIs and provides an open information system about them.

Like the CAS Registry, the MOI registry should assign numbers called \textbf{math object
  numbers} (MONs) in sequential order\footnote{Crucially, MONs should not be construed to
  carry mathematical information. In particular the MONs should be independent of any
  classification system of mathematical objects such as the MSC or other systematic
  schemes.} to (sufficiently unique descriptions of) mathematical objects identified by
the mathematical community for inclusion in the database. From these numbers the MOI
registry derives \textbf{Math Object Identifiers} (MOIs): unique string identifiers
(hashes) that may contain check digits or related safety measures.

In principle any \emph{persistent, peer-reviewd description} of a mathematical concept,
object, or model is eligible for registration as long as it is sufficiently different from
already registered ones. We restrict ourselves to mathematical objects whose descriptions
have been published in some kind of persistent, peer-reviewed medium for three reasons:
\begin{compactenum}[\em i\rm)]
\item the set of mathematical objects, even the named ones is infinite (it includes the
  natural numbers,
\item we want to select out the set of objects that are of interest to the mathematical
  community -- interesting enough to have passed peer review, and
\item unless they have been published in a persistent -- i.e. immutable -- medium, we
  cannot guarantee that MOIs form persistent references
\end{compactenum}

Upon registration of a MOI, the Registry establishes a \textbf{Math Object Record} (MOR),
which contains contains the mandatory fields
\begin{compactenum}
\item the math object number (\texttt{MON}), 
\item the registration date (\texttt{MORDate})
\item a normative reference to the media fragment that constitutes the MO description --
  we call this a \textbf{math object description reference} (\texttt{MODRef}).
\end{compactenum}
Other metadata can be added directly in the MOR or by linked-open-data methods, but is not
considered normative. We will give examples in the next section.

The exact criteria for ensuring peer-review and persistence -- the latter may well include
versioned \texttt{MODRef}s into versioned media -- should be determined by the MOI
registry organization.

\section{The Demo}

We have developed a {\LaTeX} packge for annotating MOI information in the publication and
review process, and tested this on the CICM processin 2018. While the return in actual
annotations has been unsatisfactory, we attribute this to a somewhat late organization and
a confusion on the part of authors what a MOI actually is. 

This demo intends to change that by elucidating the concepts in time for submissions for
CICM 2019. Details about MOIs can be found at ~\cite{Kohlhase:moitrdm17}, the {\LaTeX}
package is at~\cite{moi.sty:on},

\printbibliography
\end{document}
%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:

%  LocalWords:  maketitle oeis Sloane:oeis12 Gap-RefMan:on lmfdb:on,Cremona:LMFDB16 mizar
%  LocalWords:  formalized Harrison:hlti96 GonAspAiv:mcpoot13 HalAdaBau:afpkc15 zbMATH:on
%  LocalWords:  MathSciNet:on zbMATH MathSciNet KohMihSperTes:mfs13 sec:handles DOI:on
%  LocalWords:  bibliometry InChI:on CASregistry:on standardized axiomatizations textbf
%  LocalWords:  Sloane:oeis12,oeis Cremona:LMFDB16,lmfdb:on centering hline fig:doimoi
%  LocalWords:  lmfdb fig:archmoi lstinline thm fig:localmoi numpy:mandelbrot numpy emph
%  LocalWords:  formalizations printbibliography Kohlhase:tffm13 sec:moi-proposal IMKT:on
%  LocalWords:  sec:concl organization IMU:on OMSoc:on peer-reviewd fig:tplmoi llncs
%  LocalWords:  standardize retro-digitized MathHub:on,IanJucKoh:sdm14 MueGauKal:cacfms17
%  LocalWords:  LuzKoh:fsarfo16 standardization BusCapCar:2oms04 EngBraDav:ubce13 amsthm
%  LocalWords:  sec:appl mathbf mathbf sec:ex sec:doimoi hdashline includegraphics Leyer
%  LocalWords:  referencible sec:moi-arch omcd-core:on Bressert:snod12 thefootnote
%  LocalWords:  organizational
