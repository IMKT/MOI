# Math Object Identifiers

We develop a system of fine-grained registration-based identifiers for mathemalical objects.

* `doc`: contains documentation; most prominently `doc/proposal/proposal.pdf`, our current thoughts
* `registration_data`: and example data set to prototype our systems
* `sty`: LaTeX support for the reviewing and publication process with MOIs
