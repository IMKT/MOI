Some MO Registration Requests  (MORR) in JSON and XML forms from the LWDA17 paper (files with corresponding names should be (kept to be) equivalent. 

* `moi.jsc`: the beginnings of a JSON schema (see http://json-schema.org/specification.html) 
* `MOIRegs.rnc`: A RelaxNG schema for the XML; this works much better than the JSC schema and should be considered as the primary validation soure. 
* `DOI.json/xml`: a set of inter-related MORRs
* `archive.json/xml`: MORRs for objects in mathematical data sources. 
* `tplib.json/mxml`: MORRs for objects in a theorem prover library.


In `DOI.json`, the `<MOI:?>` are references to MOIs that will be given to the objects in the MORR file itself. They will be replaced with registered MOIs throughout the registered data. The `MON`, `MOI`, and `MODDate` fields are normally supplied by the MOI registrar upon registration. The fact that the `MOI` field is already filled out in the request is only to trigger the substitution mechanism (there may be other substitutions in the future). 

